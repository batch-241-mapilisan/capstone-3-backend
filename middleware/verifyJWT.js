const jwt = require('jsonwebtoken')

const verifyJWT = (req, res, next) => {
    const authHeader = req.headers.authorization || req.headers.Authorization

    if (!authHeader.startsWith('Bearer ')) {
        return res.status(401).json({ message: "Unauthorized. Please log in" })
    }

    const token = authHeader.split(' ')[1]

    jwt.verify(
        token,
        process.env.ACCESS_TOKEN_SECRET,
        (error, decoded) => {
            if (error) return res.status(403).json({ message: "Forbidden" })
            req.userId = decoded.UserInfo.userId
            req.username = decoded.UserInfo.username
            req.isAdmin = decoded.UserInfo.isAdmin
            next()
        }
    )
}

module.exports = verifyJWT