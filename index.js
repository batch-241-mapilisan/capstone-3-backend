require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors')
const connectDB = require('./config/connectDb')
const authRoutes = require('./routes/AuthRoutes')
const userRoutes = require('./routes/UserRoutes')
const productRoutes = require('./routes/ProductRoutes')

const app = express()
const PORT = process.env.PORT || 4000
const db = mongoose.connection

// Connect to Database
connectDB()

// Middlewares
app.use(cors())
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// Routes
app.use('/auth', authRoutes)
app.use('/users', userRoutes)
app.use('/products', productRoutes)


db.once('open', () => {
    console.log('Connected to MongoDB')
    app.listen(PORT, () => console.log(`Server is up and running on port ${PORT}`))
})

db.on('error', error => {
    console.log(error)
})