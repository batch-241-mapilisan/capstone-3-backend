const mongoose = require('mongoose')

const productSchema = new mongoose.Schema(
    {   
        productId: {
            type: Number,
            required: true
        },
        productName: {
            type: String,
            required: true
        },
        series: {
            type: String,
            required: true
        },
        manufacturer: {
            type: String,
            required: true
        },
        category: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        releaseDate: {
            type: String,
            required: true
        },
        specifications: {
            type: String,
            required: true
        },
        isActive: {
            type: Boolean,
            default: true
        }
    }
)

const Product = mongoose.model('product', productSchema)

module.exports = Product