const User = require('../models/User')
const bcrypt = require('bcrypt')

// @desc Get All Users
// @route GET /users
// @access Private
const getAllUsers = async (req, res) => {
    const users = await User.find().select('-password')
    if (!users?.length) {
        return res.status(404).json({ message: 'No users found' })
    }
    res.status(200).json(users)
}

// @desc Get Single User
// @route GET /users/:id
// @access Public
const getUser = async (req, res) => {
    const { id } = req.params

    if (!id) {
        return res.status(400).json({ message: "User Id is required" })
    }

    const user = await User.findById(id).select("-password")

    if (!user) {
        return res.status(404).json({ message: "User not found" })
    }
    res.status(200).json(user)
}

// @desc Create New User
// @route POST /users
// @access Public
const createNewUser = async (req, res) => {
    const { username, password, isAdmin } = req.body

    // Confirm data
    if (!username || !password) {
        return res.status(400).json({ message: "All fields are required" })
    }

    // Check for duplicate
    const duplicate = await User.findOne({ username }).collation({ locale: 'en', strength: 2 })

    if (duplicate) {
        return res.status(409).json({ message: "Username is already taken. Please use a different username." })
    }

    // Hash password
    const hashedPassword = await bcrypt.hash(password, 10)
    const userObject = (isAdmin)
                        ? { username, password: hashedPassword, isAdmin }
                        : { username, password: hashedPassword }

    // Create and store new user
    const newUser = await User.create(userObject)

    if (newUser) {
        res.status(201).json({ message: `New user ${username} created`, status: 'success' })
    } else {
        res.status(400).json({ message: "Invalid user data received" })
    }
}

// @desc Update User
// @route PUT /users/:id
// @access Private
const updateUser = async (req, res) => {
    const { id } = req.params

    if (!id) {
        return res.status(400).json({ message: "User Id is required" })
    }
    
    const { username, password, isAdmin } = req.body

    // Confirm data
    if (!username) {
        return res.status(400).json({ message: "All fields are required" })
    }

    const user = await User.findById(id)

    if (!user) {
        return res.status(400).json({ message: "User not found" })
    }

    // Check for duplicate
    const duplicate = await User.findOne({ username }).collation({ locale: 'en', strength: 2 })

    if (duplicate && duplicate._id.toString() !== id) {
        return res.status(409).json({ message: "Username is already taken. Please use a different username" })
    }

    user.username = username
    user.isAdmin = isAdmin
    
    if (password) {
        user.password = await bcrypt.hash(password, 10)
    }

    const updatedUser = await user.save()

    res.status(200).json({ message: `${updatedUser.username} successfully updated` })
}

// @desc Delete User
// @route DELETE /users/:id
// @access Private
const deleteUser = async (req, res) => {
    const { id } = req.params

    if (!id) {
        return res.status(400).json({ message: "User Id is required" })
    }

    const user = await User.findById(id)

    if (!user) {
        return res.status(400).json({ message: "User not found" })
    }

    const result = await user.deleteOne()

    const reply = `Username ${result.username} deleted`

    res.status(200).json({message: reply})
}

module.exports = {
    getAllUsers,
    getUser,
    createNewUser,
    updateUser,
    deleteUser
}