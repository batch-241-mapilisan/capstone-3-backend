const express = require('express')
const { retrieveAllProducts, retrieveAllActiveProducts, retrieveSingleProduct, createProduct, updateProduct, reactivateProduct, deactivateProduct } = require('../controllers/ProductControllers')
const verifyJWT = require('../middleware/verifyJWT')

const router = express.Router()

router.route('/active')
    .get(retrieveAllActiveProducts)

router.route('/:id')
    .get(retrieveSingleProduct)

router.route('/')
    .get(retrieveAllProducts)

router.use(verifyJWT)

router.route('/:id/deactivate')
    .patch(deactivateProduct)

router.route('/:id/reactivate')
    .patch(reactivateProduct)

router.route('/:id')
    .put(updateProduct)

router.route('/')
    .post(createProduct)

module.exports = router