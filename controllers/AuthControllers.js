const User = require('../models/User')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

// @desc Login
// @route POST /auth
// @access Public
const login = async (req, res) => {
    const { username, password } = req.body

    // Verify Data
    if (!username || !password) {
        return res.status(400).json({ message: "All fields are required" })
    }

    const foundUser = await User.findOne({username})

    if (!foundUser) {
        return res.status(401).json({ message: "The username you entered isn't connected to an account" })
    }

    const match = await bcrypt.compare(password, foundUser.password)

    if (!match) return res.status(401).json({ message: "The password you've entered is incorrect." })

    const accessToken = jwt.sign(
        {
            "UserInfo": {
                "userId": foundUser._id,
                "username": foundUser.username,
                "isAdmin": foundUser.isAdmin
            }
        },
        process.env.ACCESS_TOKEN_SECRET,
        { expiresIn: '1d' }
    )
    
    // Send accessToken containing UserInfo
    res.json({accessToken, status: "success"})
}

module.exports = {
    login
}