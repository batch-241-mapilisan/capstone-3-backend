const express = require('express')
const { getAllUsers, getUser, createNewUser, updateUser, deleteUser } = require('../controllers/UserControllers')
const verifyJWT = require('../middleware/verifyJWT')

const router = express.Router()

router.route('/')
    .post(createNewUser)

router.use(verifyJWT)

router.route('/:id')
    .get(getUser)
    .put(updateUser)
    .delete(deleteUser)

router.route('/')
    .get(getAllUsers)

module.exports = router