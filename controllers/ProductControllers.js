const Product = require('../models/Product')

// @desc Retrieve All Products
// @route GET /products
// @access Public
const retrieveAllProducts = async (req, res) => {
    const products = await Product.find()

    if (!products.length) {
        return res.status(404).json({ message: "There are no products" })
    }

    res.status(200).json(products)
}

// @desc Retrieve All Active Products
// @route GET /products/active
// @access Public
const retrieveAllActiveProducts = async (req, res) => {
    const activeProducts = await Product.find({isActive: true})

    if (!activeProducts.length) {
        return res.status(404).json({ message: "There are no active products currently." })
    }

    res.status(200).json(activeProducts)
}

// @desc Retrieve Single Product
// @route GET /products/:id
// @access Public
const retrieveSingleProduct = async (req, res) => {
    const { id } = req.params

    const product = await Product.findById(id)

    if (!product) {
        return res.status(404).json({ message: "Product you're looking for doesn't exist" })
    }

    res.status(200).json(product)
}

// @desc Create Product
// @route POST /products
// @access Private (Admin)
const createProduct = async (req, res) => {
    const { productId, productName, series, manufacturer, category, price, releaseDate, specifications } = req.body

    // Confirm data
    if (!productId || !productName || !series || !manufacturer || !category || !price || !releaseDate || !specifications) {
        return res.status(400).json({ message: "All fields are required" })
    }

    // Check for duplicate
    const duplicate = await Product.findOne({productId})

    if (duplicate) {
        return res.status(409).json({ message: "Duplicate product Id" })
    }

    const productObject = { productId, productName, series, manufacturer, category, price, releaseDate, specifications }

    // Create and store new product
    const newProduct = await Product.create(productObject)

    if (newProduct) {
        res.status(200).json({ message: `New ${newProduct.productName} created`, status: "success" })
    } else {
        res.status(400).json({ message: "Invalid product data received" })
    }
}

// @desc Update Product
// @route PUT /products/:id
// @access Private (Admin)
const updateProduct = async (req, res) => {
    const { id } = req.params

    const { productId, productName, series, manufacturer, category, price, releaseDate, specifications } = req.body

    // Confirm data
    if (!productId || !productName || !series || !manufacturer || !category || !price || !releaseDate || !specifications) {
        return res.status(400).json({ message: "All fields are required" })
    }

    const product = await Product.findById(id)

    if (!product) {
        return res.status(404).json({ message: "Product not found" })
    }

    // Check for duplicate
    const duplicate = await Product.findOne({productId})

    if (duplicate && duplicate._id.toString() !== id) {
        return res.status(409).json({ message: "Duplicate product Id" })
    }

    product.productId = productId
    product.productName = productName
    product.series = series
    product.manufacturer = manufacturer
    product.category = category
    product.price = price
    product.releaseDate = releaseDate
    product.specifications = specifications

    const updatedProduct = await product.save()

    res.status(200).json({ message: `${updatedProduct.productName} successfully updated`, status: "success" })
}

// @desc Deactivate Product
// @route PATCH /products/:id/deactivate
// @access Private (Admin)
const deactivateProduct = async (req, res) => {
    const { id } = req.params

    if (!id) {
        return res.status(400).json({ message: "Product Id is required" })
    }

    const product = await Product.findById(id)

    if (!product) {
        return res.status(404).json({ message: "Product not found" })
    }

    product.isActive = false

    const updatedProduct = await product.save()

    res.status(200).json({ message: `${updatedProduct.productName} is deactivated` })
}

// @desc Reactivate Product
// @route PATCH /products/:id/reactivate
// @access Private (Admin)
const reactivateProduct = async (req, res) => {
    const { id } = req.params

    if (!id) {
        return res.status(400).json({ message: "Product Id is required" })
    }

    const product = await Product.findById(id)

    if (!product) {
        return res.status(404).json({ message: "Product not found" })
    }

    product.isActive = true

    const updatedProduct = await product.save()

    res.status(200).json({ message: `${updatedProduct.productName} is reactivated` })
}

module.exports = {
    retrieveAllProducts,
    retrieveAllActiveProducts,
    retrieveSingleProduct,
    createProduct,
    updateProduct,
    deactivateProduct,
    reactivateProduct
}